﻿using System.Collections.Generic;
using CacheManager.Domain;

namespace CacheManager
{
    public interface ICacheManager
    {
        Project GetProject(string key);
        IEnumerable<Resource> Resources { get; set; }
    }
}