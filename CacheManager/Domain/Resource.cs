﻿namespace CacheManager.Domain
{
    public class Resource
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }
}