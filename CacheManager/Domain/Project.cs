﻿namespace CacheManager.Domain
{
    public class Project
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }
}