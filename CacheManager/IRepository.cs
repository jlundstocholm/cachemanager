﻿using System.Collections.Generic;
using CacheManager.Domain;

namespace CacheManager
{
    public interface IRepository
    {
        Project GetProject(string key);
    }
}