﻿using System;
using System.Timers;

namespace CacheManager
{
    public class Runner
    {
        

        public void Run()
        {
            //var result = _cacheManager.Projects;


            _timer = new Timer();
            _timer.Elapsed += OnTimedEvent;
            _timer.Interval = 10000;
            _timer.Enabled = true;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            var random = new Random();
            var value = random.Next(1, 6);
            Console.WriteLine($"Runner:\t\tTrying to get project with key: {value}");
            var project = _cacheManager.GetProject(value.ToString());
            Console.WriteLine($"Runner:\t\tProject with name '{project.Name}' was retrieved");
        }

        public Runner(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
            _timer = new Timer();
        }
        private readonly ICacheManager _cacheManager;
        private Timer _timer;
    }
}