﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using CacheManager.Domain;

namespace CacheManager
{
    public class CacheManager : ICacheManager
    {
        /// <summary>
        /// Gets a <see cref="Project"/> from Cache
        /// </summary>
        /// <param name="key">Unique key for Project</param>
        /// <returns>A Project eintiry</returns>
        public Project GetProject(string key)
        {
            // Check if Project is already in cache
            if (_projectsCache.Contains(key))
            {
                Console.WriteLine("CacheManager:\tProject found with key: " + key);
                // return Project
                return _projectsCache.Get(key) as Project;
            }
            else
            {
                Console.WriteLine($"CacheManager:\tProject with key {key} was not in cache");
                //Get Project from repository
                var project = _repository.GetProject(key);

                // Create an UpdateCallback method with absolute expiration
                CacheEntryUpdateCallback projectCacheItemBeforeUpdateCallback = Projects_CacheItemUpdateCallback;
                var cacheItemPolicy = new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTime.Now.AddSeconds(30),
                    UpdateCallback = projectCacheItemBeforeUpdateCallback
                };

                Console.WriteLine($"CacheManager:\tSetting project with key {key} in cache");
                // Add Project from repository to Cache
                _projectsCache.Set(project.Key, project, cacheItemPolicy);

                // return Project
                return project;

            }
        }

        private void Projects_CacheItemUpdateCallback(CacheEntryUpdateArguments arguments)
        {
            Console.WriteLine($"CacheManager:\tItem in project cache with key {arguments.Key} has expired");
            var project = _repository.GetProject(arguments.Key);
            arguments.UpdatedCacheItem = new CacheItem(arguments.Key, project);
            CacheEntryUpdateCallback projectCacheItemBeforeUpdateCallback = Projects_CacheItemUpdateCallback;

            arguments.UpdatedCacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                UpdateCallback = projectCacheItemBeforeUpdateCallback
            };
        }

        public IEnumerable<Resource> Resources { get; set; }

        public CacheManager(IRepository repository)
        {
            _repository = repository;
            _projectsCache = new MemoryCache("Projects");
            _resourcesCache = new MemoryCache("Resources");
        }

        private readonly MemoryCache _projectsCache;
        private readonly MemoryCache _resourcesCache;
        private readonly IRepository _repository;
    }
}