﻿using System;
using System.Collections.Generic;
using System.Linq;
using CacheManager.Domain;

namespace CacheManager
{
    public class Repository : IRepository
    {
        public Project GetProject(string key)
        {   
            Console.WriteLine($"Repository:\tGetting project with key {key}");
            return _projects.First(i => i.Key == key);
        }

        public Repository()
        {
            _projects = new List<Project> { new Project { Key = "1", Name = "Project 1" }, new Project { Key = "2", Name = "Project 2" }, new Project { Key = "3", Name = "Project 3" }, new Project { Key = "4", Name = "Project 4" }, new Project { Key = "5", Name = "Project 5" } };
        }

        private readonly IEnumerable<Project> _projects;
    }
}