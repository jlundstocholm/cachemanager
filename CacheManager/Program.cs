﻿using System;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace CacheManager
{
    public class Program
    {
        private static void Main()
        {

            _container = new WindsorContainer().Install(FromAssembly.This());

            var runner = _container.Resolve<Runner>();

            runner.Run();

            Console.Read();

        }

        private static IWindsorContainer _container;
    }



}
