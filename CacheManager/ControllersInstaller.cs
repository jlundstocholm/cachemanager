﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace CacheManager
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Register(Component.For<Runner>())
                .Register(Component.For<ICacheManager>().ImplementedBy<CacheManager>().LifestyleSingleton())
                .Register(Component.For<IRepository>().ImplementedBy<Repository>())
                ;
        }
    }
}